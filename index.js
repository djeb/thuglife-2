/*------------------------------------------------------------------------------------------------------------ */
/*Fonction qui permet d'afficher 1 images en fonction de la valeur du random et de la valeur du 'i' 
            qu'on aura choisis*/
/*------------------------------------------------------------------------------------------------------------ */            
function addImage(balise, random, number){

    if(number == random) {
       balise.innerHTML += '<div><img src="img/gamer.jpg" style="width:100px; height:100px"></div>';
    }
    else {
       balise.innerHTML += '<div><img src="img/thugdawg.jpg" style="width:100px; height:100px"></div>';
    }
}

/*------------------------------------------------------------------------------------------------------------- */
/*Récupérer l'ID de la div qui est dans le HTML*/
/*------------------------------------------------------------------------------------------------------------- */
var wrapper = document.getElementById("wrapper");
/*Affichage de la valeur dans la console */
console.log(wrapper);

/*------------------------------------------------------------------------------------------------------------- */
/*----APPEL de la fonction avec les valeurs trouvés*/
/*------------------------------------------------------------------------------------------------------------- */

/*valeur que l'on a choisis pour comparer le nombre aléatoire' */
var number = 10;

/*boucle for pour afficher 120 une image sur la page */
for(var i=0; i<120; i++){
    /*Récupérer un nombre aléatoire entre 0 et 121*/
    var val = Math.floor(Math.random() * 121);
    /*Affichage de la valeur sur la console */
    console.log(val);
    /*Appel de la fonction add_image qui affichera une image en fonction de la valeur de val */
    addImage(wrapper, val, number);
}
